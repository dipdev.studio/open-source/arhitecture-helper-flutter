import 'package:flutter/foundation.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_arhitecture_helper/presentation/ui/mvvm/utils/base_model_utils.dart';
import 'package:flutter_arhitecture_helper/presentation/ui/mvvm/utils/base_view_utils.dart';
import 'package:flutter_arhitecture_helper/presentation/ui/widgets/loading_widget.dart';

import 'base_model.dart';

abstract class BaseView<M extends BaseModel> extends State<StatefulWidget>
    with AutomaticKeepAliveClientMixin, BaseViewUtils {
  final bool keepAlive;
  final bool viewActions;

  M _model;
  M get model => _model;

  BaseView(this._model, {this.viewActions = true, this.keepAlive = false});

  @override
  Widget build(BuildContext context) {
    super.build(context);
    Widget widget = Directionality(
        textDirection: TextDirection.ltr,
        child: new Stack(
            children: <Widget>[getView(context), getLoadingWidget()]));
    model?.viewCallbacks?.viewCreatedAction();
    model?.context = context;
    return widget;
  }

  @override
  void initState() {
    super.initState();
    if (viewActions) {
      WidgetsBinding.instance
          .addObserver(new LifecycleEventHandler(model?.viewCallbacks));
      WidgetsBinding.instance.addPostFrameCallback(
          (_) => model?.viewCallbacks?.viewInitStateAction());
    }
  }

  void updateUI([VoidCallback callback]) {
    if (mounted) {
      if (callback != null) {
        setState(callback);
      } else {
        setState(() {});
      }
    }
  }

  /// This method needs to be implemented to provide the Widget for display.
  Widget getView(BuildContext context);

  @override
  void dispose() {
    model.viewCallbacks.viewDisposedAction();
    super.dispose();
  }

  @override
  bool get wantKeepAlive => keepAlive;

  Widget getLoadingWidget() {
    return new LoadingWidget.android(showing: model.loading);
  }
}

class LifecycleEventHandler extends WidgetsBindingObserver {
  LifecycleEventHandler(this.viewCallbacks);

  final ViewCallbacks viewCallbacks;

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    switch (state) {
      case AppLifecycleState.inactive:
        this.viewCallbacks?.viewInactiveAction();
        break;
      case AppLifecycleState.paused:
        this.viewCallbacks?.viewPausedAction();
        break;
      case AppLifecycleState.detached:
        this.viewCallbacks?.viewDetachedAction();
        break;
      case AppLifecycleState.resumed:
        this.viewCallbacks?.viewResumedAction();
        break;
    }
    super.didChangeAppLifecycleState(state);
  }
}
